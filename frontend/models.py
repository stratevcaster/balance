import base64

from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Documents(models.Model):
    '''a model for a blog post'''

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )
    date_created = models.DateField(auto_now_add=True)
    document_name = models.CharField(max_length=40)
    servicio = models.CharField(max_length=100)
    comentario = models.CharField(max_length=100, null=True)


class DocumentData(models.Model):

    doc_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, models.CASCADE)
    data = models.TextField(db_column='_data', blank=True) # Lo he cogido de aqui https://djangosnippets.org/snippets/1597/
    creation_data = models.DateField(auto_now_add=True)
    comment = models.CharField(max_length=100, null=True)
    
    def set_data(self, data):
        self.data = base64.encodestring(data)

    def get_data(self):
        return base64.b64decode(self.data)

    _data = property(get_data, set_data) # Y esto https://www.programiz.com/python-programming/property
