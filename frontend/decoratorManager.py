from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import user_passes_test
import logging
logger = logging.getLogger('managerCapture')

def permission_admin_manager(perm, login_url=None, raise_exception=False):
    """
    Decorator for views that checks whether a user has a particular permission
    enabled, redirecting to the log-in page if necessary.
    If the raise_exception parameter is given the PermissionDenied exception
    is raised.
    """
    def check_perms(user):
        # First check if the user has the permission (even anon users)
        groupUser=user.groups.all().first()
        if str(groupUser) == str(perm):
            logger.debug("Group Admin ACCEPTED")
            return True
        else:
            logger.debug("Group is NOT Admin DENIED")
            return False
        
        #if user.has_perms(perms):
        #    return True
        # In case the 403 handler should be called raise the exception
        if raise_exception:
            raise PermissionDenied
        # As the last resort, show the login form
        return False
    return user_passes_test(check_perms, login_url=login_url)
