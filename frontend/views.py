
from Balance.settings import BASE_DIR, MEDIA_URL
import os
import uuid
import platform
import urllib
import traceback
import json
from urllib.parse import urlencode, quote_plus
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

from frontend.models import Documents, DocumentData
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
import logging
logger = logging.getLogger('Balancer')
audit = logging.getLogger('Audit')
from django.views.decorators.csrf import csrf_protect
import random
from frontend.decoratorManager import permission_admin_manager


@login_required
def index(request):
    logger.info("Start View")
    #TODO CPU, MEMORIA u Otros datos

    logger.info("End View")
    return render(request, 'home.html')


def user_manager(request):
    logger.info("Start View")
    users = []
    groups = []
    try:
        # Obteniendo los usuarios y grupos
        all_users = User.objects.all()
        for user in all_users:
            user_name = user.username
            user_group = user.groups.all().first()
            users.append({"username": str(user_name), "groupname": str(user_group)})

        all_groups = Group.objects.all()
        for group in all_groups:
            groups.append(str(group.name))

    except Exception as e:
        logger.error('ERROR Exception in user_manager the error is: ' + repr(e) + " - args - ")

    logger.info("End View")

    return render(request, 'user-manager.html', {"all_users": users, "all_groups": groups})

@login_required
def mainpage(request):
    logger.info("Start View")
    
    
    logger.info("End View")
    return render(request, 'document-history_v2.html')


###############
### actions ###
###############

@login_required
@csrf_protect
@permission_admin_manager("Administrator", login_url="permission_denied")
def user_register(request):
    logger.info("Start View")
    username = request.POST["username"]
    password = request.POST["password"]
    user_group = request.POST["group-selector"]
    group = Group.objects.get(name=str(user_group))
    user = User.objects.create_user(username=str(username), password=str(password))
    user.groups.add(group)
    user.save()
    logger.info("End View")
    return redirect('user_manager')


@login_required
def balancer(request):
    logger.info("Start View")
    return 


def deleteRegister(request):
    logger.info("Start View")
    logger.info("End View")
    return render(request, 'permission-denied.html')
@login_required


def permission_denied(request):
    logger.info("Start View")
    logger.info("End View")
    return render(request, 'permission-denied.html')

